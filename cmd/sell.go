package cmd

import (
	"fmt"
	"strings"

	"github.com/alpacahq/alpaca-trade-api-go/alpaca"
	"github.com/spf13/cobra"
	"gitlab.com/aenegri/toro/order"
)

type sellCmd struct {
	cmd *cobra.Command

	// flags
	qty         int
	limitPrice  float64
	stopPrice   float64
	timeInForce string
}

func newSellCmd() *sellCmd {
	sell := &sellCmd{
		cmd: &cobra.Command{
			Use:   "sell SYMBOL",
			Short: "Place an order to sell stocks",
			Args:  cobra.ExactArgs(1),
		},
	}

	sell.cmd.RunE = func(cmd *cobra.Command, args []string) error {
		client, err := getAlpacaClient()

		if err != nil {
			return err
		}

		return sell.Sell(strings.ToUpper(args[0]), client)
	}

	sell.cmd.Flags().IntVarP(&sell.qty, "quantity", "q", 1, "number of stocks to sell")
	sell.cmd.Flags().Float64VarP(&sell.limitPrice, "limit-price", "l", 0, "the minimum price to sell a share")
	sell.cmd.Flags().Float64VarP(&sell.stopPrice, "stop-price", "s", 0, "price trigger to start order")
	sell.cmd.Flags().StringVar(
		&sell.timeInForce,
		"time-in-force",
		"day",
		"indicates how long an order will remain active before it is executed or expires. One of: day|gtc|opg|cls|ioc|fok. See alpaca documentation for details [https://alpaca.markets/docs/trading-on-alpaca/orders/#time-in-force]",
	)

	return sell
}

func (s *sellCmd) GetCommand() *cobra.Command {
	return s.cmd
}

func (s *sellCmd) Sell(symbol string, seller orderer) error {
	req, err := order.NewBuilder().
		Side(alpaca.Sell).
		Symbol(symbol).
		Quantity(s.qty).
		LimitPrice(s.limitPrice).
		StopPrice(s.stopPrice).
		Build()

	if err != nil {
		return err
	}

	_, err = seller.PlaceOrder(req)

	if err != nil {
		return err
	}

	fmt.Println("✅ Order placed")

	return nil
}
